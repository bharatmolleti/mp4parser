// MP4Parser.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cstring>

typedef unsigned __int32 uint32_t;

const int PARSER_OK = 0;
const int PARSER_ERROR = -1;
const int PARSER_EOF = 1;

// the thing below denotes a single box.
class MP4Box{
private:
	long offset;
	uint32_t size;
	char name[5];
public:
	MP4Box(long offset, int size, char* name){
		this->offset = offset;
		this->size = size;
		memcpy(this->name, name, 5);
	}
	
	char* getName(){
		return name;
	}

	uint32_t* getSize(){
		return &size;
	}

	long* getOffset(){
		return &offset;
	}
};

// this is more of a function object. this does the parsing stuff.
// this thing only knows how to move inside and recognize mp4 styled boxes.
class MP4BoxParser{
private:
	FILE* pFile;
	char boxName[5];
	uint32_t boxSize;
	unsigned char t_boxSize[4];
	long currOffset;
	long prevOffset;

	int readSize(){	
		boxSize = 0;
		for(int i = 0; i < 4; i++){
			boxSize <<= 8;
			if(1 != fread(&t_boxSize[i], 1, 1, pFile))
				return PARSER_ERROR;
			boxSize += t_boxSize[i];
		}
		return PARSER_OK;
	}

	int readName(){	
		if(4 != fread(boxName, 1, 4, pFile)){
			return PARSER_ERROR;
		};
		boxName[4] = '\0';
		return PARSER_OK;
	}

public:
	MP4BoxParser(FILE* pFile, long offset){
		this->pFile = pFile;
		boxSize = 0;
		currOffset = offset;
		prevOffset = 0;
		boxName[0] = '\0';

		if(pFile) fseek(pFile, 0, SEEK_SET);
	}

	~MP4BoxParser(){
		pFile = NULL;
	}

	int parseNext(){

		if(pFile == NULL){
			return PARSER_ERROR;
		}

		prevOffset = currOffset;		

		if(feof(pFile)){
			return PARSER_EOF;
		}

		if(0!= fseek(pFile, prevOffset+boxSize, SEEK_SET)){
			return PARSER_ERROR;
		};
		currOffset = ftell(pFile);

		if(PARSER_OK != readSize()) return PARSER_ERROR;
		if(PARSER_OK != readName()) return PARSER_ERROR;

		return PARSER_OK;
	}

	char* getName(){
		return boxName;
	}

	uint32_t* getSize(){
		return &boxSize;
	}

	long* getOffset(){
		return &currOffset;
	}
};

// this knows the mp4 file structure.
class MP4Parser{
private:
	FILE *pFile;
	MP4BoxParser *reader;

	MP4Box *ftyp, *moov, *mdat;

public:
	MP4Parser(char* fileName){
		pFile = fopen(fileName, "rb");
		reader = new MP4BoxParser(pFile, 0);

		ftyp = NULL;
		moov = NULL;
		mdat = NULL;
	}

	~MP4Parser(){
		
		if(ftyp){
			delete ftyp;
		}
		ftyp = NULL;

		if(moov){
			delete moov;
		}
		moov = NULL;

		if(mdat){
			delete mdat;
		}
		mdat = NULL;

		if(reader) delete reader;
		reader = NULL;

		if(pFile) fclose(pFile);
		pFile = NULL;
	}

	int parseMain(){
		while(PARSER_OK == reader->parseNext()){
		
			printf("\nName: %s - Size: %d - Offset: %d", reader->getName(), *(reader->getSize()), *(reader->getOffset()));

			if(strcmp(reader->getName(), "ftyp")== 0){
				ftyp = new MP4Box(*(reader->getOffset()),  *(reader->getSize()), reader->getName());
			}
			else if(strcmp(reader->getName(), "moov")== 0){
				moov = new MP4Box(*(reader->getOffset()),  *(reader->getSize()), reader->getName());
			}
			else if(strcmp(reader->getName(), "mdat")== 0){
				mdat = new MP4Box(*(reader->getOffset()),  *(reader->getSize()), reader->getName());
			}
		}
		return PARSER_OK;
	}

	int parseMOOV(){
		MP4BoxParser _reader(pFile, *(moov->getOffset())+8);
		printf("\n Reader started @ offset : %d ", *(moov->getOffset())+8);
		while(PARSER_OK == _reader.parseNext()){			
			if( *(_reader.getOffset()) >= (*(moov->getOffset()) + *(moov->getSize()))){
				return PARSER_EOF;
			}
			printf("\nMOOV Name: %s - Size: %d - Offset: %d", _reader.getName(), *(_reader.getSize()), *(_reader.getOffset()));
		}
		return PARSER_OK;
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	char* sFileName = "test.mp4";
	
	MP4Parser parser(sFileName);

	parser.parseMain();

	parser.parseMOOV();

	return 0;
}

